<?php
// class induk
class malik {
  
   public $gender;
   public $hobi;
   public $pekerjaan;

   public function __construct()
   {
       echo "Hallo";
   }
  
   public function namaLengkap() {
     return "Ridwan Malik";
   }

   public function __destruct()
   {
       echo "Sampai Jumpa";
   }
}
  
// class turunan
class biodata extends malik {
  
   public function data() {
     return "gender: $this->gender, hobi: $this->hobi, pekerjaan: $this->pekerjaan";
   }
}
  
// buat objek dari class biodata (instansiasi)
$ridwanMalik = new biodata();
  
// isi property objek
$ridwanMalik->gender = "laki-laki";
$ridwanMalik->hobi ="sepak bola, membaca";
$ridwanMalik->pekerjaan = "programmer";
  
//panggil method objek
echo $ridwanMalik->namaLengkap();
echo $ridwanMalik->data();


// protected
class malikV2 {
  
    protected $gender = "laki-laki";
    protected $hobi = "sepak bola, membaca";
    protected $pekerjaan = "programmer";

    public function genderMethod() {
        return $this->gender;
    }

    public function hobiMethod() {
        return $this->hobi;
    }

    public function pekerjaanMethod() {
        return $this->pekerjaan;
    }
 }

 class biodataV2 extends malikV2{
     public function pekerjaan(){
         return $this->pekerjaan;
     }
 }

 $ridwanMalikV2 = new malikV2();
 echo $ridwanMalikV2->genderMethod();
 echo $ridwanMalikV2->hobiMethod();
 echo $ridwanMalikV2->pekerjaanMethod();

 $biodataMalik = new biodataV2();
 $biodataMalik->pekerjaan();


 //private
 class malikV3 {
  
    private $gender = "laki-laki";
    private $hobi = "sepak bola, membaca";
    private $pekerjaan = "programmer";

    public function genderMethod() {
        return $this->gender;
    }

    public function hobiMethod() {
        return $this->hobi;
    }

    public function pekerjaanMethod() {
        return $this->pekerjaan;
    }
 }

 $ridwanMalikV3 = new malikV3();
 echo $ridwanMalikV3->genderMethod();
 echo $ridwanMalikV3->hobiMethod();
 echo $ridwanMalikV3->pekerjaanMethod();

